# Canuda_EdGB_dec

# Author:    Helvi Witek, Matthew Elley, Noora Ghadiri, Hector O. Silva
# Developer: Helvi Witek, Matthew Elley, Noora Ghadiri, Hector O. Silva

# Purpose:
Code capable to simulate black holes in Gauss-Bonnet gravity in an EFT approach up to first order in the coupling.
Compatible with the Einstein Toolkit

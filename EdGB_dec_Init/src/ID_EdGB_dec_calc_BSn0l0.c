/*=======================================================*/
/*     intial data thorn: ID_EdGB_dec_calc_BSn0l0.c      */
/*=======================================================*/

#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <ctype.h>
#include "cctk.h"
#include "cctk_Arguments.h"
#include "cctk_Parameters.h"
#include "ID_EdGB_dec_utils.h"

/* -------------------------------------------------------------------*/
void ID_EdGB_dec_calc_BSn0l0(CCTK_ARGUMENTS);

void
ID_EdGB_dec_calc_BSn0l0(CCTK_ARGUMENTS)
{
  /* This initial data should be used only in combination with a
   * Schwarzschild BH background.
   */

  DECLARE_CCTK_ARGUMENTS;
  DECLARE_CCTK_PARAMETERS;

  CCTK_INFO("=== Begin ID_EdGB_dec_BSn0l0 initial data ===");

  /*=== define parameters ====================================*/
  /* Fitting constant values ci and black hole mass mp */
  const CCTK_REAL c1 = 0.460469;
  const CCTK_REAL c2 = 0.155388;
  const CCTK_REAL c3 = 1.796390;
  const CCTK_REAL mp = m_plus;
  /*==========================================================*/

  /*=== define grid length ===================================*/
  CCTK_INT imin[3], imax[3];
  for (int d = 0; d < 3; ++ d)
  {
    imin[d] = 0;
    imax[d] = cctk_lsh[d];
  }
  /*==========================================================*/

  /*=== loops over full grid =================================*/
  /*----------------------------------------------------------*/
  //#pragma omp parallel for
  for (int k = imin[2]; k < imax[2]; ++k)
  {
   for (int j = imin[1]; j < imax[1]; ++j)
   {
    for (int i = imin[0]; i < imax[0]; ++i)
    {

     const int ind = CCTK_GFINDEX3D (cctkGH, i, j, k);

     /*=== initialize grid functions as zero ==================*/
     Phi_gf[ind]  = 0.0;
     KPhi_gf[ind] = 0.0;
     /*========================================================*/

     /*=== initialize local functions as zero =================*/
     CCTK_REAL phi, Kphi;
     phi  = 0.0;
     Kphi = 0.0;
     /*========================================================*/

     /*=== define radius ===========================*/
     // positions
     // HS: added option to include an offset
     CCTK_REAL xp[3];
     xp[0] = x[ind] - pos_plus[0];
     xp[1] = y[ind] - pos_plus[1];
     xp[2] = z[ind] - pos_plus[2];

     // coordinate radius and polar radial coordinate
     CCTK_REAL rr, rr2;
     rr = sqrt( xp[0] * xp[0] + xp[1] * xp[1] + xp[2] * xp[2] );
     if( rr < eps_r ) rr = eps_r;
     rr2 = rr*rr;
     /*========================================================*/

     /*=== calculate bound state fit ==========================*/

     // Analytical fit to \ell = n = 0 bound state solution
     // for the scalar field in a Schwarzschild background in
     // the f \propto phi^2 theory

     phi = ( (8.0 * mp * rr) / pow( mp + 2.0*rr, 2) )
           * ( c1 + (4.0*c2*mp*rr) / pow( mp + 2.0*rr, 2)
                  + (16.0*c3*mp*mp*rr2) /  pow( mp + 2.0*rr, 4));
     Kphi = 0.0;

     /*========================================================*/

     /*=== write to grid functions ============================*/
     Phi_gf[ind]  = phi;
     KPhi_gf[ind] = Kphi;

     /*========================================================*/

    } /* for i */
   }  /* for j */
  }   /* for k */
  /*=== end of loops over grid =================================*/
  /*============================================================*/

  CCTK_INFO("=== End ID_EdGB_dec_BSn0l0 initial data ===");

}
/* -------------------------------------------------------------------*/

# Parameter definitions for thorn EdGB_dec_Evol
# $Header:$

#=== pars from/for ADMBase ==================
SHARES: ADMBase

#=== pars from EdGB_dec_Base ================
SHARES: EdGB_dec_Base

EXTENDS KEYWORD evolution_method "evolution method"
{
  EdGB_dec_Evol :: ""
}

USES KEYWORD   sGB_coupling
USES CCTK_REAL fPhi_beta2
USES CCTK_REAL fPhi_lambda
USES KEYWORD   sGB_potential
USES CCTK_REAL mu_phi
USES CCTK_REAL f_dil
USES CCTK_REAL Phi0
USES CCTK_REAL KPhi0
USES CCTK_INT  n_Phi
USES CCTK_INT  n_KPhi
USES CCTK_REAL RL

#=== pars from MoL ==========================
SHARES: MethodOfLines
USES CCTK_INT MoL_Num_Evolved_Vars
USES CCTK_INT MoL_Num_Constrained_Vars
USES CCTK_INT MoL_Num_SaveAndRestore_Vars


#=== local parameters ==========================
restricted:

CCTK_INT EdGB_dec_MaxNumEvolvedVars "The maximum number of evolved variables used by EdGB_dec_Evol" ACCUMULATOR-BASE=MethodofLines::MoL_Num_Evolved_Vars
{
  2:2           :: "Phi(1), KPhi(1)"
} 2

CCTK_INT EdGB_dec_MaxNumConstrainedVars "The maximum number of constrained variables used by EdGB_dec_Evol" ACCUMULATOR-BASE=MethodofLines::MoL_Num_Constrained_Vars
{
  0:0         :: "None"
} 0

CCTK_INT EdGB_dec_MaxNumSandRVars "The maximum number of save and restore variables used by EdGB_dec_Evol" ACCUMULATOR-BASE=MethodofLines::MoL_Num_SaveAndRestore_Vars
{
  16:16           :: "ADM vars (16)"
} 16


private:
# auxiliary parameters
#============================================

CCTK_REAL eps_r "Minimal value of radius"
{
  0:*  :: "Any value possible"
} 1.0d-06

BOOLEAN calculate_EdGB_Tmunu "Calculate the effective energy-momentum tensor in scalar Gauss--Bonnet gravity?"
{
} "yes"


BOOLEAN calculate_norms "Calculate the norm of the metric and energy-momentum tensor?"
{
} "no"


# Parameters related to the particular structure of the WBSSN equations
#=====================================================================
CCTK_REAL WW_floor "Minimal value of WW_gf"
{
  *:*  :: "Any value possible"
} 1.0d-04

BOOLEAN impose_WW_floor "Use floor value on initial data?"
{
} "yes"

CCTK_INT derivs_order "Order for derivatives"
{
  4 :: "4th order stencils"
  6 :: "6th order stencils"
} 4


BOOLEAN use_advection_stencils "Use lop-sided stencils for advection derivs"
{
} "yes"

# Parameters for multipatch implementation
#=====================================================================
BOOLEAN z_is_radial "use with multipatch"
{
} "no"


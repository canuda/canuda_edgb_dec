
#include "cctk.h"
#include "cctk_Arguments.h"

subroutine EdGB_dec_zero_rhs( CCTK_ARGUMENTS )

  implicit none
  DECLARE_CCTK_ARGUMENTS

  rhs_Phi_gf  = 0
  rhs_KPhi_gf = 0

end subroutine EdGB_dec_zero_rhs


#------------------------------------------------------------------------------
ActiveThorns = "
  ADMBase
  ADMCoupling
  ADMMacros
  AEILocalInterp
  AHFinderDirect
  Boundary
  Carpet
  CarpetIOASCII
  CarpetIOBasic
  CarpetIOHDF5
  CarpetIOScalar
  CarpetInterp
  CarpetLib
  CarpetMask
  CarpetReduce
  CarpetRegrid2
  CarpetTracker
  CartGrid3D
  CoordBase
  CoordGauge
  Dissipation
  EdGB_dec_Base
  EdGB_dec_Evol
  EdGB_dec_Init
  Fortran
  GenericFD
  GSL
  HDF5
  InitBase
  IOUtil
  KerrQuasiIsotropic
  LocalInterp
  LoopControl
  MoL
  Multipole
  NaNChecker
  NewRad
  QuasiLocalMeasures
  ReflectionSymmetry
  Slab
  SpaceMask
  SphericalSurface
  StaticConformal
  SymBase
  SystemStatistics
  TerminationTrigger
  Time
  TimerReport
  TmunuBase
"
#------------------------------------------------------------------------------


# Grid setup
#------------------------------------------------------------------------------

CartGrid3D::type                     = "coordbase"
Carpet::domain_from_coordbase        = yes
CoordBase::domainsize                = "minmax"

# make sure all (xmax - xmin)/dx are integers!
CoordBase::xmin                      =    0.00
CoordBase::ymin                      =    0.00
CoordBase::zmin                      =    0.00
CoordBase::xmax                      =  256.00
CoordBase::ymax                      =  256.00
CoordBase::zmax                      =  256.00
CoordBase::dx                        =   1.0
CoordBase::dy                        =   1.0
CoordBase::dz                        =   1.0

driver::ghost_size                   = 3

CoordBase::boundary_size_x_lower     = 3
CoordBase::boundary_size_y_lower     = 3
CoordBase::boundary_size_z_lower     = 3
CoordBase::boundary_size_x_upper     = 3
CoordBase::boundary_size_y_upper     = 3
CoordBase::boundary_size_z_upper     = 3

CoordBase::boundary_shiftout_x_lower = 1
CoordBase::boundary_shiftout_y_lower = 1
CoordBase::boundary_shiftout_z_lower = 1

ReflectionSymmetry::reflection_x     = yes
ReflectionSymmetry::reflection_y     = yes
ReflectionSymmetry::reflection_z     = yes
ReflectionSymmetry::avoid_origin_x   = no
ReflectionSymmetry::avoid_origin_y   = no
ReflectionSymmetry::avoid_origin_z   = no

Time::dtfac                             = 0.225
Carpet::time_refinement_factors         = "[1, 1, 2, 4, 8, 16, 32, 64, 128, 256, 512]"


# Mesh refinement
#------------------------------------------------------------------------------

Carpet::max_refinement_levels           = 8

CarpetRegrid2::num_centres              = 1

CarpetRegrid2::num_levels_1             = 8
CarpetRegrid2::radius_1[1]              = 128.0
CarpetRegrid2::radius_1[2]              =  32.0
CarpetRegrid2::radius_1[3]              =  16.0
CarpetRegrid2::radius_1[4]              =   6.0
CarpetRegrid2::radius_1[5]              =   3.0
CarpetRegrid2::radius_1[6]              =   1.5
CarpetRegrid2::radius_1[7]              =   0.6

Carpet::use_buffer_zones                = yes
Carpet::prolongation_order_space        = 5
Carpet::prolongation_order_time         = 2

CarpetRegrid2::freeze_unaligned_levels  = yes
CarpetRegrid2::regrid_every             = -1

CarpetRegrid2::verbose                  = no

Carpet::grid_structure_filename         = "carpet-grid-structure"
Carpet::grid_coordinates_filename       = "carpet-grid-coordinates"


# Initial Data
#------------------------------------------------------------------------------

ADMBase::initial_data                 = "KQI_ana"
ADMBase::initial_lapse                = "KQI_ana"
ADMBase::initial_shift                = "KQI_ana"
ADMBase::initial_dtlapse              = "zero"
ADMBase::initial_dtshift              = "zero"

ADMBase::lapse_timelevels             = 3
ADMBase::shift_timelevels             = 3
ADMBase::metric_timelevels            = 3

KerrQuasiIsotropic::m_plus            = 1.0
KerrQuasiIsotropic::pos_plus[0]       = 1.0d-04
KerrQuasiIsotropic::spin_plus         = 0.0
KerrQuasiIsotropic::eps_r             = 1.0d-06

EdGB_dec_Init::schedule_in_ADMBase_InitialData = no
EdGB_dec_Base::EdGB_dec_initdata      = "ID_EdGB_dec_Gaussian"
EdGB_dec_Init::EdGB_dec_GaussType     = "SF_Gauss00"
EdGB_dec_Init::ampSF                  =  1.0
EdGB_dec_Init::r0                     =  6.0
EdGB_dec_Init::width                  =  1.0



InitBase::initial_data_setup_method   = "init_some_levels"
Carpet::init_fill_timelevels          = yes
Carpet::init_3_timelevels             = no

#InitBase::initial_data_setup_method   = "init_all_levels"
#Carpet::init_fill_timelevels          = no
#Carpet::init_3_timelevels             = yes

# Evolution
#------------------------------------------------------------------------------

ADMBase::evolution_method               = "static"
ADMBase::lapse_evolution_method         = "static"
ADMBase::shift_evolution_method         = "static"
ADMBase::dtlapse_evolution_method       = "static"
ADMBase::dtshift_evolution_method       = "static"

ADMBase::lapse_prolongation_type        = "none"
ADMBase::shift_prolongation_type        = "none"
ADMBase::metric_prolongation_type       = "none"


EdGB_dec_Base::evolution_method         = "EdGB_dec_Evol"
EdGB_dec_Base::RL                       = 1.0
EdGB_dec_Base::sGB_coupling             = "quadratic"
EdGB_dec_Base::fPhi_beta2               = 0.5
EdGB_dec_Evol::impose_WW_floor          = yes
EdGB_dec_Evol::WW_floor                 = 1.0d-04
EdGB_dec_Evol::derivs_order             = 4
EdGB_dec_Evol::use_advection_stencils   = "yes"
##EdGB_dec_Evol::calculate_densities      = "yes"

# Spatial finite differencing
#------------------------------------------------------------------------------

Dissipation::epsdis = 0.15
Dissipation::order  = 5
Dissipation::vars   = "
  EdGB_dec_Base::Phi_gf
  EdGB_dec_Base::KPhi_gf
"


# Integration method
#------------------------------------------------------------------------------
MoL::ODE_Method                 = "RK4"
MoL::MoL_Intermediate_Steps     = 4
MoL::MoL_Num_Scratch_Levels     = 1

Carpet::num_integrator_substeps = 4


# Spherical surfaces
#------------------------------------------------------------------------------

SphericalSurface::nsurfaces = 1
SphericalSurface::maxntheta = 66
SphericalSurface::maxnphi   = 124
SphericalSurface::verbose   = no

# Horizon
SphericalSurface::ntheta            [0] = 41
SphericalSurface::nphi              [0] = 80
SphericalSurface::nghoststheta      [0] = 2
SphericalSurface::nghostsphi        [0] = 2
CarpetMask::excluded_surface        [0] = 0
CarpetMask::excluded_surface_factor [0] = 1.0

CarpetMask::verbose = no


# Wave extraction
#------------------------------------------------------------------------------

Multipole::nradii       = 8
Multipole::radius[0]    =  0.5
Multipole::radius[1]    =  10
Multipole::radius[2]    =  20
Multipole::radius[3]    =  40
Multipole::radius[4]    =  60
Multipole::radius[5]    = 100
Multipole::radius[6]    = 150
Multipole::radius[7]    = 200
Multipole::nphi         = 240
Multipole::integration_method = Simpson
Multipole::variables    = "
  EdGB_dec_Base::Phi_gf{sw=0 name='Phi'}
"

Multipole::l_max        = 2
Multipole::out_every    = 64
Multipole::output_hdf5  = no
Multipole::output_ascii = yes


# Horizons
#------------------------------------------------------------------------------

# AHFinderDirect::verbose_level                           = "algorithm highlights"
AHFinderDirect::verbose_level                            = "physics details"
AHFinderDirect::output_BH_diagnostics                    = "true"
AHFinderDirect::run_at_CCTK_POST_RECOVER_VARIABLES       = no

AHFinderDirect::N_horizons                               = 1
AHFinderDirect::find_every                               = 100000

AHFinderDirect::output_h_every                           = 0
AHFinderDirect::max_Newton_iterations__initial           = 50
AHFinderDirect::max_Newton_iterations__subsequent        = 50
AHFinderDirect::max_allowable_Theta_growth_iterations    = 10
AHFinderDirect::max_allowable_Theta_nonshrink_iterations = 10
AHFinderDirect::geometry_interpolator_name               = "Lagrange polynomial interpolation"
AHFinderDirect::geometry_interpolator_pars               = "order=4"
AHFinderDirect::surface_interpolator_name                = "Lagrange polynomial interpolation"
AHFinderDirect::surface_interpolator_pars                = "order=4"

AHFinderDirect::move_origins                             = no

AHFinderDirect::origin_x                             [1] = 0
AHFinderDirect::initial_guess__coord_sphere__x_center[1] = 0
AHFinderDirect::initial_guess__coord_sphere__radius  [1] = 0.25
AHFinderDirect::which_surface_to_store_info          [1] = 0
AHFinderDirect::set_mask_for_individual_horizon      [1] = no
AHFinderDirect::reset_horizon_after_not_finding      [1] = no
AHFinderDirect::max_allowable_horizon_radius         [1] = 3


# Isolated Horizons
#-------------------------------------------------------------------------------

QuasiLocalMeasures::verbose                = yes
QuasiLocalMeasures::veryverbose            = no
QuasiLocalMeasures::interpolator           = "Lagrange polynomial interpolation"
QuasiLocalMeasures::interpolator_options   = "order=4"
QuasiLocalMeasures::spatial_order          = 4
QuasiLocalMeasures::num_surfaces           = 1
QuasiLocalMeasures::surface_index      [0] = 0


# Check for NaNs
#-------------------------------------------------------------------------------

Carpet::poison_new_timelevels = no
CarpetLib::poison_new_memory  = no
Carpet::check_for_poison      = no

NaNChecker::check_every     = 512
NanChecker::check_after     = 0
NaNChecker::report_max      = 10
NaNChecker::verbose         = "all"
NaNChecker::action_if_found = "terminate"
NaNChecker::out_NaNmask     = yes
NaNChecker::check_vars      = "
  EdGB_dec_Base::Phi_gf
"


# Timers
#-------------------------------------------------------------------------------

Cactus::cctk_timer_output               = "full"
TimerReport::out_every                  = 5120
TimerReport::n_top_timers               = 40
TimerReport::output_all_timers_together = yes
TimerReport::output_all_timers_readable = yes
TimerReport::output_schedule_timers     = no


# I/O thorns
#-------------------------------------------------------------------------------

Cactus::cctk_run_title       = $parfile
IO::out_dir                  = $parfile

IOScalar::one_file_per_group = yes
IOASCII::one_file_per_group  = yes
IOHDF5::one_file_per_group   = no
IOHDF5::use_checksums        = no

IOBasic::outInfo_every       = 16
IOBasic::outInfo_reductions  = "minimum maximum"
IOBasic::outInfo_vars        = "
  Carpet::physical_time_per_hour
  EdGB_dec_Base::Phi_gf
  SystemStatistics::maxrss_mb
"

# # for scalar reductions of 3D grid functions
IOScalar::outScalar_every               = 64
IOScalar::all_reductions_in_one_file    = "yes"
IOScalar::outScalar_reductions          = "minimum maximum sum average norm1 norm2 norm_inf"
IOScalar::outScalar_vars                = "
  EdGB_dec_Base::Phi_gf
##  EdGB_dec_Evol::EdGB_dec_Tmn_scalar
"

# IOASCII output
IOASCII::output_symmetry_points = no
IOASCII::out3D_ghosts           = no

# output just at one point (0D)
IOASCII::out0D_every = 128
IOASCII::out0D_vars  = "
  Carpet::timing
  QuasiLocalMeasures::qlm_scalars{out_every = 100000}
  EdGB_dec_Base::Phi_gf
  EdGB_dec_Evol::GBinv_gf{out_every = 128}
"

# 1D text output
IOASCII::out1D_every            = 128
IOASCII::out1D_d                = no
IOASCII::out1D_x                = yes
IOASCII::out1D_y                = no
IOASCII::out1D_z                = yes
IOASCII::out1D_vars             = "
  ADMBase::lapse{out_every = 100000}
  ADMBase::shift{out_every = 100000}
  ADMBase::metric{out_every = 100000}
  ADMBase::curv{out_every = 100000}
  EdGB_dec_Base::Phi_gf
  EdGB_dec_Base::KPhi_gf
  EdGB_dec_Evol::GBinv_gf
"

# 1D HDF5 output
#IOHDF5::out1D_every            = 256
#IOHDF5::out1D_d                = no
#IOHDF5::out1D_x                = yes
#IOHDF5::out1D_y                = no
#IOHDF5::out1D_z                = no
#IOHDF5::out1D_vars             = "
#  ADMBase::lapse
#"

# 2D HDF5 output
#IOHDF5::out2D_every             = 256
#IOHDF5::out2D_xy                = yes
#IOHDF5::out2D_xz                = no
#IOHDF5::out2D_yz                = no
#IOHDF5::out2D_vars              = "
#  EdGB_dec_Base::Phi_gf
#  EdGB_dec_Evol::GBinv_gf{out_every = 2560}
###  EdGB_dec_Evol::rhoSF_gf
#"

# # 3D HDF5 output
# IOHDF5::out_every                      = 8192
# IOHDF5::out_vars                       = "
#   ADMBase::lapse
# "

Carpet::verbose                    = no
Carpet::veryverbose                = no
Carpet::schedule_barriers          = no
Carpet::storage_verbose            = no
CarpetLib::output_bboxes           = no

Cactus::cctk_full_warnings         = yes
Cactus::highlight_warning_messages = no


# Checkpointing and recovery
#-------------------------------------------------------------------------------
CarpetIOHDF5::checkpoint             = yes
IO::checkpoint_every_walltime_hours  = 12
IO::checkpoint_dir                   = "checkpoints_SBH_a0_Y00_r6_Quad_bet05_KQIana_RL8_dx1"
IO::checkpoint_keep                  = 1
IO::checkpoint_ID                    = no
IO::checkpoint_on_terminate          = yes

IO::recover                          = "autoprobe"
IO::recover_dir                      = "checkpoints_SBH_a0_Y00_r6_Quad_bet05_KQIana_RL8_dx1"

IO::abort_on_io_errors                      = yes
CarpetIOHDF5::open_one_input_file_at_a_time = yes
CarpetIOHDF5::compression_level             = 9


# Run termination
#-------------------------------------------------------------------------------
TerminationTrigger::max_walltime                 = 24 # hours
TerminationTrigger::on_remaining_walltime        = 30 # minutes
TerminationTrigger::output_remtime_every_minutes = 30

Cactus::terminate       = "time"
Cactus::cctk_final_time = 300.0
